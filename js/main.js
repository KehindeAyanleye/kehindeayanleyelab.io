const second = 1000,minute = second * 60,hour = minute * 60,day = hour * 24;

let countDown = new Date("February 23, 2019 23:59:00").getTime(),
    x = setInterval(function() {
    let now = new Date().getTime(),
        distance = countDown - now;
        (document.getElementById("days").textContent = Math.floor(distance / day)),
        (document.getElementById("hours").innerText = Math.floor(
        (distance % day) / hour
    )),
        (document.getElementById("minutes").innerText = Math.floor(
        (distance % hour) / minute
    )),
        (document.getElementById("seconds").innerText = Math.floor(
        (distance % minute) / second
    ));
});